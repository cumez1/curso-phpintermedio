<?php

//Escribir en un fichero

$archivo = fopen("archivo.txt", "w"); // Abre el archivo en modo escritura
fwrite($archivo, "Cambiar de contenido\nOtra Linea"); // Escribe el texto en el archivo
fclose($archivo); // Cierra el archivo
die();



//Leer un fichero

$archivo = fopen("archivo.txt", "r"); // Abre el archivo en modo lectura
if ($archivo) { // Verifica que el archivo se haya abierto correctamente
    while (($linea = fgets($archivo)) !== false) { // Lee el archivo línea por línea
        echo $linea . "<br>"; // Imprime cada línea del archivo
    }

    fclose($archivo); // Cierra el archivo
} else {
  echo "No se pudo abrir el archivo";
}

die();


//Agregar datos a un archivo existente:

$archivo = fopen("archivo.txt", "a"); // Abre el archivo en modo escritura al final
fwrite($archivo, "\nTexto que se agrega al final del archivo"); // Escribe el texto en el archivo
fclose($archivo); // Cierra el archivo


