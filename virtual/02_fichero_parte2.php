<?php

$archivo = fopen("ficheros_ejemplo/datos.csv", "r"); // Abre el archivo CSV en modo lectura

if ($archivo) { // Verifica que el archivo se haya abierto correctamente
    echo "<table>";
    while (($datos = fgetcsv($archivo, 1000, ",")) !== false) { // Lee el archivo CSV
        echo "<tr>";
        foreach ($datos as $dato) {
            echo "<td>" . $dato . "</td>"; // Imprime cada dato en una celda de la tabla
        }
        echo "</tr>";
    }
    echo "</table>";
    fclose($archivo); // Cierra el archivo
} else {
    echo "No se pudo abrir el archivo";
}