<?php
/*
//Obtener la fecha y hora actual:

$fecha_actual = new DateTime();
echo $fecha_actual->format('d/m/Y H:i:s');

//TimeZone
echo '<br>';

$zone = date_default_timezone_get();
echo $zone;

die();

//TimeZone
$zone = date_default_timezone_get();

echo $zone;

date_default_timezone_set('America/Guatemala');

echo '<br>';

$zone = date_default_timezone_get();

echo $zone;


//Crear una fecha a partir de una cadena:
//Y-m-d
//dd/mm/yyyy

$fecha = DateTime::createFromFormat('d/m/Y', '31/01/2023');
echo $fecha->format('Y-m-d H:i:s');

die();




//Obtener la diferencia de días entre dos fechas:

$fecha1 = new DateTime('2022-04-01');
$fecha2 = new DateTime('2022-04-10');
$diferencia = $fecha1->diff($fecha2)->format('%a');
echo $diferencia;

die();


//Sumar días a una fecha:
$fecha = new DateTime('2023-05-09');
$fecha->modify('+7 days');
echo $fecha->format('Y-m-d');


die();

//Restar días a una fecha:
$fecha = new DateTime('2022-04-08');
$fecha->modify('-3 days');
echo $fecha->format('Y-m-d');

die();

//Obtener el número de días de un mes:
$mes = 2;
$anio = 2020;
$num_dias = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
echo $num_dias;

*/

//Obtener numero de semana segun fecha
$fecha = new DateTime('2023-05-15');
echo $fecha->format('W');

