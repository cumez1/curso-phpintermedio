
<?php include ('head.php');?>
<?php include ('funciones.php');?>

<?php
    
    $numero1 = (float) ($_GET['numero1'] ?? 0);
    $numero2 = (float) ($_GET['numero2'] ?? 0);
    
    $resultado =  $numero1 + $numero2;

?>

<div class="container">
  <div class="row">
    <div class="col">
        <?php if ($resultado > 0) :?>
          RESULTADO: <?= $resultado ?>
        <?php endif; ?>
    </div>
  </div>
</div>