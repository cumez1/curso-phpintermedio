<form method="GET" action="action.php">
  <div class="row mb-4">
    <div class="col">
      <div class="input-control">
        <label class="form-label" for="numero1">Número 1</label>
        <input type="text" id="numero1" name="numero1" class="form-control" />
      </div>
    </div>
    <div class="col">
      <div class="input-control">
        <label class="form-label" for="numero2">Número 2</label>
        <input type="text" id="numero2" name="numero2" class="form-control" />
      </div>
    </div>
  </div>

  <!-- Submit button -->
  <button type="submit" class="btn btn-primary btn-block mb-4">Suma</button>
</form>
