<?php

function debug($params, $die = true)  
{
    echo "<div class='card text-bg-danger p-2 m-2'>";
    echo "<div class='card-header'>Debug</div>";
        echo "<pre>";
        echo var_dump($params);
        echo "</pre>";
    echo "</div>";

    if ($die) die();

}

?>