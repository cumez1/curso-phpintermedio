<?php


//Leer un archivo JSON y mostrar los datos en una lista HTML:

$datos = file_get_contents("ficheros_ejemplo/datos.json"); // Lee el archivo JSON y almacena los datos en una variable
$datos = json_decode($datos, true); // Decodifica los datos JSON en un array asociativo


echo "<table>";
echo "<tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Edad</th>
        <th>Genero</th>
    </tr>";

    foreach ($datos as $dato) {
        echo '<tr>';
        foreach($dato as $value) {
            echo '<td>'.$value.'</td>';
        }
        echo '</tr>';
    }

echo "</table>";

