
<?php 
    $titulo = 'Introducción a Formularios en PHP';
    $arrayPaises = ['Mexico', 'Guatemala', 'Honduras', 'El Salvador', 'Costa Rica', 'Panama'];
?>

<?php include ('head.php'); ?>
<?php include ('funciones.php');?>

<div class="container">
    <div class="row">
        <div class="col-6">
            <h1>Introducción a Formulario </h1>
            
            <div class="card mr-t-10">
                <?php require ('formulario.php'); ?>
            </div>
        </div>
    </div>
</div>    


<?php require ('footer.php'); ?>