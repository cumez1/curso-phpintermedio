<form method="POST" action="action.php">
  <div class="row mb-4">
    
    <div class="col">
      <div class="input-control">
        <label class="form-label" for="nombre">Nombre</label>
        <input type="text" id="nombre" name="nombre" class="form-control" />
      </div>
    </div>

    <div class="col">
      <div class="input-control">
        <label class="form-label" for="apellido">Apellido</label>
        <input type="text" id="apellido" name="apellido" class="form-control" />
      </div>
    </div>
    
  </div>

  <div class="input-control mb-4">
    <label class="form-label" for="correo">Correo</label>
    <input type="email" id="correo" name="correo" class="form-control"/>
  </div>

  <div class="input-control mb-4">
    <label class="form-label" for="password">Contraseña</label>
    <input type="password" id="password" name="password" class="form-control" />
  </div>

  <!-- Submit button -->
  <button type="button" class="btn btn-primary btn-block mb-4">Regresar a la pagina anterior</button>
  <button type="submit" class="btn btn-primary btn-block mb-4">Guardar Registro</button>
</form>